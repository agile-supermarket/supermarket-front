function openModal(id) {
  $("#" + id).modal('show');
}

function closeModal(id) {
  $("#" + id).modal('toggle');
}

function showLoading() {
  $("#modal_loading").modal({
    backdrop: 'static',
    keyboard: false
  });
}

function closeLoading() {
  $("#modal_loading").modal('toggle');
}

function openAlert(title, message, confirm, cancel, type, callback) {
  swal({
      title: title,
      text: message,
      type: type, //success,warning,error,info
      allowOutsideClick: false,
      showConfirmButton: confirm.length != 0 ? true : false,
      showCancelButton: cancel.length != 0 ? true : false,
      // confirmButtonClass: '#6777ef',
      // cancelButtonClass: 'red',
      closeOnConfirm: true,
      closeOnCancel: true,
      confirmButtonText: confirm,
      cancelButtonText: cancel,
    },
    function (isConfirm) {
      if (callback != undefined)
        callback(isConfirm);
    });
}
