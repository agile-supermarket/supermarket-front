import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {ServiceBase} from './service.base';
import {Sale} from '../model/sale';

@Injectable({
  providedIn: 'root'
})
export class SaleService extends ServiceBase {

  public SALE_API = this.BASE_URL + '/sale';

  constructor(http: HttpClient) {
    super(http);
  }

  getAll(): Observable<any> {
    const url = `${this.SALE_API}/`;
    // console.log('Endpoint: ', url);
    return this.http.get(url, this.HTTP_OPTIONS);
  }
}
