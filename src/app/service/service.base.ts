import {Settings} from '../settings';
import {HttpClient, HttpHeaders} from '@angular/common/http';

export class ServiceBase {

  public BASE_URL = Settings.BASE_URL;
  public HTTP_OPTIONS = {
    headers: new HttpHeaders({
      'Authorization': 'Basic ' + sessionStorage.getItem('token')
    })
  };
  
  constructor(public http: HttpClient) {}
}
