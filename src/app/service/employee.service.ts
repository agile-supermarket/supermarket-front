import {Injectable} from '@angular/core';
import {ServiceBase} from './service.base';
import {User} from '../model/user';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class EmployeeService extends ServiceBase {

  public EMPLOYEE_API = this.BASE_URL + '/employee';

  constructor(http: HttpClient) {
    super(http);
  }

  getAll(): Observable<any> {
    const url = `${this.EMPLOYEE_API}/`;
    return this.http.get(url,  this.HTTP_OPTIONS);
  }

  get(id: string): Observable<any> {
    const url = `${this.EMPLOYEE_API}/${id}`;
    return this.http.get(url, this.HTTP_OPTIONS);
  }

  add(model: User) {
    const url = `${this.EMPLOYEE_API}/`;
    return this.http.post(url, model, this.HTTP_OPTIONS);
  }

  update(model: User) {
    const url = `${this.EMPLOYEE_API}/` + model.id;
    return this.http.put(url, model, this.HTTP_OPTIONS);
  }

  remove(id: string) {
    const url = `${this.EMPLOYEE_API}/${id}`;
    return this.http.delete(url, this.HTTP_OPTIONS);
  }

  login(name: string, password: string) {
    const url = `http://localhost:8080/login`;
    return this.http.post(url, {name: name, password: password});
  }

}
