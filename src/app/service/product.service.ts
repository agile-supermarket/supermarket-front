import {Injectable} from '@angular/core';
import {ServiceBase} from './service.base';
import {HttpClient} from '@angular/common/http';
import {Product} from '../model/product';
import {Observable} from 'rxjs';

@Injectable()
export class ProductService extends ServiceBase {

  public PRODUCT_API = this.BASE_URL + '/product';

  constructor(http: HttpClient) {
    super(http);
  }

  getAll(search): Observable<any> {
    const url = search.term.length !== 0 || search.category.length !== 0 ? `${this.PRODUCT_API}/?name=${search.term}&category=${search.category}`
      : `${this.PRODUCT_API}/`;
    return this.http.get(url, this.HTTP_OPTIONS);
  }

  get(id: string): Observable<any> {
    const url = `${this.PRODUCT_API}/${id}`;
    return this.http.get(url, this.HTTP_OPTIONS);
  }

  add(model: Product) {
    const url = `${this.PRODUCT_API}`;
    return this.http.post(url, model, this.HTTP_OPTIONS);
  }

  update(model: Product) {
    const url = `${this.PRODUCT_API}/${model.id}`;
    return this.http.put(url, model, this.HTTP_OPTIONS);
  }

  remove(id: string) {
    const url = `${this.PRODUCT_API}/${id}`;
    return this.http.delete(url, this.HTTP_OPTIONS);
  }

  _getAll(): Observable<any> {
    const url = `${this.PRODUCT_API}/`;
    return this.http.get(url);
  }

}
