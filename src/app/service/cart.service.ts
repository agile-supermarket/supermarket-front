import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Product} from '../model/product';
import {HttpClient} from '@angular/common/http';
import {BaseComponent} from '../base/basecomponent';
import {ServiceBase} from './service.base';
import {CartProduct} from '../model/cart-product';
import {Sale} from '../model/sale';

@Injectable({
  providedIn: 'root'
})
export class CartService extends ServiceBase {

  public CART_API = this.BASE_URL + '/cart';
  public PRODUCT_API = this.BASE_URL + '/product';
  public SALE_API = this.BASE_URL + '/sale';

  constructor(http: HttpClient) {
    super(http);
  }

  getProduct(barcode: string): Observable<any> {
    const url = `${this.PRODUCT_API}/?barcode=${barcode}&limit=1`;
    return this.http.get(url, this.HTTP_OPTIONS);
  }

  add(model: CartProduct) {
    const url = `${this.CART_API}/`;
    return this.http.post(url, model, this.HTTP_OPTIONS);
  }

  _add(id: string, model: CartProduct) {
    const url = `${this.CART_API}/${id}`;
    return this.http.post(url, model, this.HTTP_OPTIONS);
  }

  remove(id: string) {
    const url = `${this.CART_API}/remove/${id}`;
    return this.http.delete(url, this.HTTP_OPTIONS);
  }

  addSale(model: Sale) {
    const url = `${this.SALE_API}/`;
    return this.http.post(url, model, this.HTTP_OPTIONS);
  }

}
