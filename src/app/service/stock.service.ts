import { Injectable } from '@angular/core';
import {ServiceBase} from './service.base';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Product} from '../model/product';
import {Stock} from '../model/stock';

@Injectable({
  providedIn: 'root'
})
export class StockService extends ServiceBase {

  public STOCK_API = this.BASE_URL + '/stock';
  public PRODUCT_API = this.BASE_URL + '/product';

  constructor(http: HttpClient) {
    super(http);
  }

  getAllProduct(): Observable<any> {
    const url = `${this.PRODUCT_API}/`;
    // console.log('Endpoint: ', url);
    return this.http.get(url, this.HTTP_OPTIONS);
  }

  getAll(): Observable<any> {
    const url = `${this.STOCK_API}/`;
    // console.log('Endpoint: ', url);
    return this.http.get(url, this.HTTP_OPTIONS);
  }

  add(model: Stock) {
    const url = `${this.STOCK_API}`;
    return this.http.post(url, model, this.HTTP_OPTIONS);
  }

}
