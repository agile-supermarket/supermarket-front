import {isUndefined} from 'util';
import {Router} from '@angular/router';
import {User} from '../model/user';

declare function openModal(id: string, options: string): any;

declare function closeModal(id: string): any;

declare function showLoading(): any;

declare function closeLoading(): any;

declare function openAlert(title: string, message: string, confirm: string, cancel: string, type: string, callback: any): any;

export class BaseComponent {

  constructor(public router: Router) {
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user')) as User;
  }

  setUser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  onOpenModal(id: string) {
    if (isUndefined(openModal)) {
      return;
    }
    openModal(id, '');
  }

  onCloseModal(id: string) {
    if (isUndefined(closeModal)) {
      return;
    }
    closeModal(id);
  }

  onShowLoading() {
    if (isUndefined(showLoading)) {
      return;
    }
    showLoading();
  }

  onCloseLoading() {
    if (isUndefined(closeLoading)) {
      return;
    }
    closeLoading();
  }

  showAlert(title: string, message: string, confirm: string, cancel: string, type: string, callback: any) {
    // success,warning,error,info
    if (isUndefined(openAlert)) {
      return;
    }
    openAlert(title, message, confirm, cancel, type, callback);
  }

  onError(error) {
    if (error.status === 401) {
      this.router.navigate(['/login']);
    }
  }

  generateId(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
