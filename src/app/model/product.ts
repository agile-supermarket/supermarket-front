export class Product {

  id: string;
  name: string;
  date_start: string;
  date_end: string;
  amount: number;
  price: number;

  constructor() {
  }
}
