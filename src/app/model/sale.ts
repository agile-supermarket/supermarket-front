import {User} from './user';
import {Cart} from './cart';

export class Sale {

  id: string;
  employee: User;
  cart: Cart;
  payments: string;
  total: number;

  constructor() {
    this.total = 0;
    this.cart = new Cart();
  }

}
