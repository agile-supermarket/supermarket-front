import {CartProduct} from './cart-product';

export class Cart {

  id: string;
  items: CartProduct[];
  total: number;

  constructor() {
    this.items = [];
    this.total = 0;
  }
}
