export class User {

  id: string;
  name: string;
  email: string;
  password: string;
  admin: boolean;
  
  constructor() {
    this.admin = false;
  }

}
