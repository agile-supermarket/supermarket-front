import {Product} from './product';

export class Stock {

  quantity: number;
  product: Product[];

  constructor() {
    this.quantity = 0;
    this.product = [];
  }

}
