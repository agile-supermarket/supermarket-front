import {Product} from './product';

export class CartProduct {

  id: string;
  product: Product;
  quantity: number;
  price: number;

  constructor() {
    this.quantity = 0;
    this.price = 0;
  }
}
