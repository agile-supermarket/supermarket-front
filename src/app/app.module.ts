import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './view/login/login.component';
import {MainComponent} from './view/main/main.component';
import {EmployeeComponent} from './view/employee/employee.component';
import {HomeComponent} from './view/home/home.component';
import {AuthGuard} from './service/auth.guard.service';
import {ProductComponent} from './view/product/product.component';
import {CartComponent} from './view/cart/cart.component';
import {FormsModule} from '@angular/forms';
import {SaleComponent} from './view/sale/sale.component';
import { StockComponent } from './view/stock/stock.component';
import { TestComponent } from './view/test/test.component';
import { CouponListComponent } from './view/coupon-list/coupon-list.component';
import {NumberFormatterDirective} from './directive/number-formatter.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    EmployeeComponent,
    HomeComponent,
    ProductComponent,
    CartComponent,
    SaleComponent,
    StockComponent,
    TestComponent,
    CouponListComponent,
    NumberFormatterDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
