import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BaseComponent} from '../../base/basecomponent';
import {EmployeeService} from '../../service/employee.service';
import {User} from '../../model/user';
import {isUndefined} from 'util';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  providers: [EmployeeService]
})
export class MainComponent extends BaseComponent implements OnInit {

  // username = sessionStorage.getItem('username');
  model: User = super.getUser();

  constructor(public router: Router,
              public employeeService: EmployeeService) {
    super(router);
  }

  ngOnInit() {
  }

  editProfile() {
    // console.log('editando');
    super.onOpenModal('modal_profile');
  }

  onSave() {
    if (isUndefined(this.model.name)) {
      super.showAlert('Atenção', 'Insira o nome', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.email)) {
      super.showAlert('Atenção', 'Insira o email', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.password)) {
      super.showAlert('Atenção', 'Insira a senha', 'OK', '', 'warning', null);
      return;
    }

     console.log('salvando');
     //super.onShowLoading();
     this.employeeService.update(this.model).subscribe(
       data => {
         // super.onCloseLoading();
         sessionStorage.setItem(
           'token',
           btoa(this.model.name + ':' + this.model.password)
         );
         super.setUser(this.model);
         super.onCloseModal('modal_profile');
         super.showAlert('Sucesso', 'Funcionário alterado com sucesso!', 'OK', '', 'success', null);
       },
       error => super.onError(error)
     );
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
