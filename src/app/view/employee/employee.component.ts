import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../../base/basecomponent';
import {Router} from '@angular/router';
import {User} from '../../model/user';
import {isUndefined} from 'util';
import {EmployeeService} from '../../service/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [EmployeeService]
})
export class EmployeeComponent extends BaseComponent implements OnInit {

  listEmployee: User[] = [];
  model: User = new User();

  constructor(public router: Router,
              public employeeService: EmployeeService) {
    super(router);
  }

  ngOnInit() {
     this.getAllEmployee();
  }

  getAdmin(admin: boolean) {
    if (admin) {
      return 'Sim';
    } else {
      return 'Não';
    }
  }

  getEmployee(id: string) {
    // super.onShowLoading();
    this.employeeService.get(id).subscribe(
      data => {
        // super.onCloseLoading();
        this.model = data as User;
      },
      error => super.onError(error)
    );
  }

  getAllEmployee() {
    // super.onShowLoading();
    this.employeeService.getAll().subscribe(
      data => {
        // super.onCloseLoading();
        this.listEmployee = data as User[];
      },
      error => super.onError(error)
    );
  }

  newEmployee() {
    // console.log('criando');
    this.model = new User();
    super.onOpenModal('modal_employee');
  }

  editEmployee(model: User) {
    // console.log('editando');
    this.model = model;
    super.onOpenModal('modal_employee');
  }

  onSave() {
    if (isUndefined(this.model.name)) {
      super.showAlert('Atenção', 'Insira o nome do funcionário', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.email)) {
      super.showAlert('Atenção', 'Insira o email do funcionário', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.password)) {
      super.showAlert('Atenção', 'Insira a senha do funcionário', 'OK', '', 'warning', null);
      return;
    }

    // console.log('salvando');
    // PRECISA VALIDAR O MODELO AINDA
    if (isUndefined(this.model.id)) {
      this.addEmployee();
    } else {
      this.updateEmployee();
    }
  }

  addEmployee() {
     //super.onShowLoading();
     this.employeeService.add(this.model).subscribe(
       data => {
         // super.onCloseLoading();
         super.onCloseModal('modal_employee');
         this.getAllEmployee();
         super.showAlert('Sucesso', 'Funcionário inserido com sucesso!', 'OK', '', 'success', null);
       },
       error => super.onError(error)
     );
  }

  updateEmployee() {
     //super.onShowLoading();
     this.employeeService.update(this.model).subscribe(
       data => {
         // super.onCloseLoading();
         super.onCloseModal('modal_employee');
         this.getAllEmployee();
         super.showAlert('Sucesso', 'Funcionário alterado com sucesso!', 'OK', '', 'success', null);
       },
       error => super.onError(error)
     );
  }

  onRemoveEmployee(id: string) {
    const self = this;
    super.showAlert('Atenção', 'Tem certeza que deseja remover este funcionário?',
      'Sim, tenho certeza', 'Não', 'info', function (confirm) {
        if (confirm) {
          self.removeEmployee(id);
        }
      });
  }

  removeEmployee(id: string) {
     //super.onShowLoading();
     this.employeeService.remove(id).subscribe(
       data => {
         // super.onCloseLoading();
         this.getAllEmployee();
         super.showAlert('Sucesso', 'Funcionário excluido com sucesso!', 'OK', '', 'success', null);
       },
       error => super.onError(error)
     );
  }
}
