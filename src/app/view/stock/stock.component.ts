import {Component, OnInit} from '@angular/core';
import {BaseComponent} from '../../base/basecomponent';
import {Router} from '@angular/router';
import {Stock} from '../../model/stock';
import {StockService} from '../../service/stock.service';
import {Product} from '../../model/product';
import {isUndefined} from 'util';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent extends BaseComponent implements OnInit {

  listStock: Stock[] = [];
  model: Stock = new Stock();

  product: string = '';
  listProduct: Product[] = [];

  constructor(public router: Router,
              public stockService: StockService) {
    super(router);
  }

  ngOnInit() {
    this.getAllProduct();
    this.getAllStock();
  }

  newStock() {
    // console.log('criando');
    this.model = new Stock();
    super.onOpenModal('modal_stock');
  }

  getAllProduct() {
    // super.onShowLoading();
    this.stockService.getAllProduct().subscribe(
      data => {
        // super.onCloseLoading();
        this.listProduct = data.content as Product[];
      },
      error => super.onError(error)
    );
  }

  getAllStock() {
    // super.onShowLoading();
    this.stockService.getAll().subscribe(
      data => {
        // super.onCloseLoading();
        this.listStock = data.content as Stock[];
      },
      error => super.onError(error)
    );
  }

  onSave() {
    if (isUndefined(this.model.quantity) || this.model.quantity === 0) {
      super.showAlert('Atenção', 'Insira a quantidade do produto', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.product) || this.product.length === 0) {
      super.showAlert('Atenção', 'Selecione o produto', 'OK', '', 'warning', null);
      return;
    }

    const self = this;
    this.listProduct.forEach(function (element) {
      if (element.id === self.product) {
        self.model.product.push(element);
      }
    });

    // console.log('salvando');
    this.addStock();
  }

  addStock() {
    // super.onShowLoading();
    this.stockService.add(this.model).subscribe(
      data => {
        // super.onCloseLoading();
        this.product = '';
        this.getAllStock();
        super.onCloseModal('modal_stock');
        super.showAlert('Sucesso', 'Produto inserido no estoque com sucesso!', 'OK', '', 'success', null);
      },
      error => super.onError(error)
    );
  }

}
