import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BaseComponent} from '../../base/basecomponent';
import {ProductService} from '../../service/product.service';
import {isUndefined} from 'util';
import {Product} from '../../model/product';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ProductService]
})
export class ProductComponent extends BaseComponent implements OnInit {

  listProduct: Product[] = [];
  model: Product = new Product();

  search = {
    term: '',
    category: ''
  };

  constructor(public router: Router,
              public productService: ProductService) {
    super(router);
  }

  ngOnInit() {
     this.getAllProduct();
  }

  getProduct(id: string) {
    // super.onShowLoading();
    this.productService.get(id).subscribe(
      data => {
        // super.onCloseLoading();
        this.model = data as Product;
      },
      error => super.onError(error)
    );
  }

  getAllProduct() {
    // super.onShowLoading();
    this.productService.getAll(this.search).subscribe(
      data => {
        // super.onCloseLoading();
        this.listProduct = data.content as Product[];
      },
      error => super.onError(error)
    );
  }

  newProduct() {
    // console.log('criando');
    this.model = new Product();
    super.onOpenModal('modal_product');
  }

  editProduct(model: Product) {
    // console.log('editando');
    this.model = model;
    super.onOpenModal('modal_product');
  }

  onSave() {
    if (isUndefined(this.model.name)) {
      super.showAlert('Atenção', 'Insira o nome do cupom', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.date_start)) {
      super.showAlert('Atenção', 'Selecione a data de início do cupom', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.date_end)) {
      super.showAlert('Atenção', 'Selecione a data de término do cupom', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.amount)) {
      super.showAlert('Atenção', 'Insira a quantidade do cupom', 'OK', '', 'warning', null);
      return;
    }

    if (isUndefined(this.model.price)) {
      super.showAlert('Atenção', 'Insira o preço do cupom', 'OK', '', 'warning', null);
      return;
    }

    // console.log('salvando');
    // PRECISA VALIDAR O MODELO AINDA
    if (isUndefined(this.model.id)) {
      this.addProduct();
    } else {
      this.updateProduct();
    }
  }

  addProduct() {

     //super.onShowLoading();
     this.productService.add(this.model).subscribe(
       data => {
         // super.onCloseLoading();
         this.getAllProduct();
         super.onCloseModal('modal_product');
         super.showAlert('Sucesso', 'Produto inserido com sucesso!', 'OK', '', 'success', null);
       },
       error => super.onError(error)
     );
  }

  updateProduct() {

     //super.onShowLoading();
     this.productService.update(this.model).subscribe(
       data => {
         // super.onCloseLoading();
         this.getAllProduct();
         super.onCloseModal('modal_product');
         super.showAlert('Sucesso', 'Produto alterado com sucesso!', 'OK', '', 'success', null);
       },
       error => super.onError(error)
     );
  }

  onRemoveProduct(id: string) {
    const self = this;
    super.showAlert('Atenção', 'Tem certeza que deseja remover este cupom?',
      'Sim, tenho certeza', 'Não', 'info', function (confirm) {
        if (confirm) {
          self.removeProduct(id);
        }
      });
  }

  removeProduct(id: string) {
    let self = this;
    this.listProduct.forEach(function (element, index) {
      if (id === element.id) {
        self.listProduct.splice(index, 1);
      }
    });
    super.showAlert('Sucesso', 'Cupom excluido com sucesso!', 'OK', '', 'success', null);
    // super.onShowLoading();
    // this.productService.remove(id).subscribe(
    //   data => {
    //     // super.onCloseLoading();
    //     this.getAllProduct();
    //     super.showAlert('Sucesso', 'Produto excluido com sucesso!', 'OK', '', 'success', null);
    //   },
    //   error => super.onError(error)
    // );
  }

}
