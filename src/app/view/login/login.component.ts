import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {EmployeeService} from '../../service/employee.service';
import {isUndefined} from 'util';
import {BaseComponent} from '../../base/basecomponent';
import {User} from '../../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [EmployeeService]
})
export class LoginComponent extends BaseComponent implements OnInit {

  optionLogin: string = 'home_login'; // home_login, login_employee, login_user and register_user

  constructor(public router: Router,
              public employeeService: EmployeeService) {
    super(router);
  }

  ngOnInit() {
  }

  changeLogin(option: string) {
    this.optionLogin = option;
  }

  openCouponList() {
    this.router.navigate(['/coupon_list']);
  }

  loginEmployee(name: string, password: string) {
    if (name.length === 0) {
      super.showAlert('Atenção', 'Insira o nome', 'OK', '', 'warning', null);
      return;
    }

    if (password.length === 0) {
      super.showAlert('Atenção', 'Insira a senha', 'OK', '', 'warning', null);
      return;
    }

    console.log(name + password);
    this.employeeService.login(name, password).subscribe(
      data => {
        if (data['permission']) {
          sessionStorage.setItem(
            'token',
            btoa(name + ':' + password)
          );
          const user: User = data['employee'] as User;
          super.setUser(user);
          this.router.navigate(['/employee']);
        } else {
          super.showAlert('Atenção', 'Usuário não encntrado', 'OK', '', 'warning', null);
        }
      },
    );
  }

  loginUser(name: string, password: string) {
    if (name.length === 0) {
      super.showAlert('Atenção', 'Insira o nome', 'OK', '', 'warning', null);
      return;
    }

    if (password.length === 0) {
      super.showAlert('Atenção', 'Insira a senha', 'OK', '', 'warning', null);
      return;
    }


    console.log(name + password);
    this.employeeService.login(name, password).subscribe(
      data => {
        if (data['permission']) {
          sessionStorage.setItem(
            'token',
            btoa(name + ':' + password)
          );
          const user: User = data['employee'] as User;
          super.setUser(user);
          this.router.navigate(['/product']);
        } else {
          super.showAlert('Atenção', 'Usuário não encntrado', 'OK', '', 'warning', null);
        }
      },
    );
  }

  registerUser(name: string, email: string, cpf: string, password: string) {
    if (name.length === 0) {
      super.showAlert('Atenção', 'Insira o nome', 'OK', '', 'warning', null);
      return;
    }

    if (email.length === 0) {
      super.showAlert('Atenção', 'Insira o email', 'OK', '', 'warning', null);
      return;
    }

    if (cpf.length === 0) {
      super.showAlert('Atenção', 'Insira o CPF', 'OK', '', 'warning', null);
      return;
    }

    if (password.length === 0) {
      super.showAlert('Atenção', 'Insira a senha', 'OK', '', 'warning', null);
      return;
    }

    this.router.navigate(['/employee']);
  }

}
