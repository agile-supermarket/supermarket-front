import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BaseComponent} from '../../base/basecomponent';
import {Sale} from '../../model/sale';
import {SaleService} from '../../service/sale.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent extends BaseComponent implements OnInit {

  listSale: Sale[] = [];
  model: Sale = new Sale();

  constructor(public router: Router,
              public saleService: SaleService) {
    super(router);
  }

  ngOnInit() {
    this.getAllSale();
  }

  newSale() {
    this.router.navigate(['/cart']);
  }

  detailSale(sale: Sale) {
    this.model = sale;
    super.onOpenModal('modal_sale');
  }

  getAllSale() {
    // super.onShowLoading();
    this.saleService.getAll().subscribe(
      data => {
        // super.onCloseLoading();
        this.listSale = data.content as Sale[];
        // console.log(this.listSale);
      },
      error => super.onError(error)
    );
  }

}
