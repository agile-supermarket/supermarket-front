import {Component, OnInit} from '@angular/core';
import {Product} from '../../model/product';
import {ProductService} from '../../service/product.service';
import {Router} from '@angular/router';
import {BaseComponent} from '../../base/basecomponent';

@Component({
  selector: 'app-coupon-list',
  templateUrl: './coupon-list.component.html',
  styleUrls: ['./coupon-list.component.css'],
  providers: [ProductService]
})
export class CouponListComponent extends BaseComponent implements OnInit {

  listProduct: Product[] = [new Product()];

  constructor(public router: Router,
              public productService: ProductService) {
    super(router);
  }

  ngOnInit() {
    // this.getAllCoupon();
  }

  getAllCoupon() {
    this.productService._getAll().subscribe(
      data => {
        this.listProduct = data.content as Product[];
      },
      error => super.onError(error)
    );
  }

}
