import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BaseComponent} from '../../base/basecomponent';
import {Cart} from '../../model/cart';
import {Product} from '../../model/product';
import {isUndefined} from 'util';
import {CartProduct} from '../../model/cart-product';
import {CartService} from '../../service/cart.service';
import {Sale} from '../../model/sale';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers: [CartService]
})
export class CartComponent extends BaseComponent implements OnInit {

  model: Cart = new Cart();
  sale: Sale = new Sale();
  product: Product = new Product();
  quantity: number = 0;
  barcode: string = '';

  constructor(public router: Router,
              public cartService: CartService) {
    super(router);
  }

  ngOnInit() {
  }

  onSearchProduct() {
    if (isUndefined(this.barcode)) {
      super.showAlert('Atenção', 'Insira o barcode do produto', 'OK', '', 'warning', null);
      return;
    }

    this.cartService.getProduct(this.barcode).subscribe(
      data => {
        if (data !== null) {
          this.product = data as Product;
        } else {
          super.showAlert('Atenção', 'Producto não encontrado', 'OK', '', 'warning', null);
        }
      },
      error => super.onError(error)
    );
  }

  onAddProductCart() {
    if (isUndefined(this.product.id)) {
      super.showAlert('Atenção', 'Encntre o produto atrvés do barcode', 'OK', '', 'warning', null);
      return;
    }

    if (this.quantity.toString().length === 0 || this.quantity === 0) {
      super.showAlert('Atenção', 'Insira a quantidade', 'OK', '', 'warning', null);
      return;
    }

    const cartProduct: CartProduct = new CartProduct();
    cartProduct.product = this.product;
    cartProduct.quantity = this.quantity;
    cartProduct.price = this.product.price * this.quantity;

    this.clearPanel();

    if (this.model.id) {
      this._addProductCart(cartProduct);
    } else {
      this.addProductCart(cartProduct);
    }
  }

  clearPanel() {
    this.barcode = '';
    this.quantity = 0;
    this.product = new Product();
  }

  addProductCart(cartProduct: CartProduct) {
    this.cartService.add(cartProduct).subscribe(
      data => {
        this.model = data as Cart;
      },
      error => super.onError(error)
    );
  }

  _addProductCart(cartProduct: CartProduct) {
    this.cartService._add(this.model.id, cartProduct).subscribe(
      data => {
        this.model = data as Cart;
      },
      error => super.onError(error)
    );
  }

  onRemoveCartProduct(id: string) {
    const self = this;
    super.showAlert('Atenção', 'Tem certeza que deseja remover este item do carrinho?',
      'Sim, tenho certeza', 'Não', 'info', function (confirm) {
        if (confirm) {
          self.removeCartProduct(id);
        }
      });
  }

  removeCartProduct(id: string) {
    this.cartService.remove(id).subscribe(
      data => {
        this.model = data as Cart;
      },
      error => super.onError(error)
    );
  }

  finishSale() {
    if (this.model.items.length === 0) {
      super.showAlert('Atenção', 'Adicione um produto no carrinho', 'OK', '', 'warning', null);
      return;
    }
    super.onOpenModal('modal_cart');
  }

  clearGeneral() {
    this.barcode = '';
    this.quantity = 0;
    this.product = new Product();
    this.model = new Cart();
    this.sale = new Sale();
  }

  onSaveSale() {
    if (isUndefined(this.sale.payments)) {
      super.showAlert('Atenção', 'Selecione uma forma de pagamento', 'OK', '', 'warning', null);
      return;
    }

    this.sale.cart = this.model;
    this.sale.total = this.model.total;
    this.sale.employee = super.getUser();

    this.cartService.addSale(this.sale).subscribe(
      data => {
        this.clearGeneral();
        super.onCloseModal('modal_cart');
        super.showAlert('Sucesso', 'Venda efetuado com sucesso!', 'OK', '', 'success', null);
      },
      error => super.onError(error)
    );
  }
}
