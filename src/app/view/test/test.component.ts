import {Component, OnInit} from '@angular/core';
import {isNumber} from 'util';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  sum(num1, num2): number {
    return num1 + num2;
  }

  sum2(num1, num2): number {
    return num1 + num2;
  }

}
