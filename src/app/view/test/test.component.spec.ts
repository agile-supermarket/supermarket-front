import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import { TestComponent } from './test.component';

describe('TestComponent', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Soma efetuado com sucesso', () => {
    expect(component.sum(8, 9)).toBeTruthy();
  });

  it('Número 1 inválido', () => {
    expect(component.sum('kkkkkk', 9)).toBeTruthy();
  });

  it('Número 2 inválido', () => {
    expect(component.sum(8, 'kkkkkk')).toBeTruthy();
  });

  it('Número 2 inválido', () => {
    expect(component.sum2('KKKKKKK', 'kkkkkk')).toBeTruthy();
  });
});
