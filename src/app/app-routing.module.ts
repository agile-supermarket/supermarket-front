import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './view/main/main.component';
import {LoginComponent} from './view/login/login.component';
import {EmployeeComponent} from './view/employee/employee.component';
import {AuthGuard} from './service/auth.guard.service';
import {HomeComponent} from './view/home/home.component';
import {ProductComponent} from './view/product/product.component';
import {CartComponent} from './view/cart/cart.component';
import {SaleComponent} from './view/sale/sale.component';
import {StockComponent} from './view/stock/stock.component';
import {CouponListComponent} from './view/coupon-list/coupon-list.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'coupon_list', component: CouponListComponent},
    {
      path: '',
      component: MainComponent,
      canActivate: [AuthGuard],
      children: [
        {path: '', component: HomeComponent, canActivate: [AuthGuard]},
        {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
        {path: 'employee', component: EmployeeComponent, canActivate: [AuthGuard]},
        {path: 'product', component: ProductComponent, canActivate: [AuthGuard]},
        {path: 'sale', component: SaleComponent, canActivate: [AuthGuard]},
        {path: 'cart', component: CartComponent, canActivate: [AuthGuard]},
        {path: 'stock', component: StockComponent, canActivate: [AuthGuard]},
      ]
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
