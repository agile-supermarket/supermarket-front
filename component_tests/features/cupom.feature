# language: pt

Funcionalidade: Novo cupom
  
  Cenário: Cadastrar novo cupom sucesso
    Dado que eu esteja na tela de cupom
    Quando eu clicar no botão novo cupom
    Então eu devo visualizar a modal de cadastro
  
