MODAL_SUCCESS_TEXT = 'Funcionário inserido com sucesso!'

Dado("eu esteja na página de funcionário") do
  visit 'http://localhost:4200/employee'
  @create_employee_page = CreateEmployeePage.new
end

Quando('eu clico em novo funcionário') do
  find('.btn.btn-secondary').click
end

E('a modal de cadastro de funcionário é mostrada') do
  find('.modal-header', :wait => 10).visible?
end

E("preencho todos os campos") do
  find('[data-test-employee-name]', :wait => 20).click
  #fill_in 'name', :with => 'funcionário'
  @create_employee_page.fill_input('name', 'funcionário')
  @create_employee_page.fill_input('email', 'email@email.com')
  @create_employee_page.fill_input('password', 'senha')
  @create_employee_page.click_event('#admin')
end

E("clico em salvar") do
  @create_employee_page.click_event('[data-test-employee-save-btn]')
end

Então('eu quero que a modal de sucesso seja exibida') do |text|
  result = @create_employee_page.verify_success_modal
  expect(result).to eq text 
end

# Erro
E('não preencho o campo nome') do 
  @create_employee_page.fill_input('email', 'email@email.com')
  @create_employee_page.fill_input('password', 'senha')
  @create_employee_page.click_event('#admin')
end


Então("eu quero que a modal de warning seja exibida") do |text|
  result = @create_employee_page.verify_warning_modal
  expect(result).to eq text 
end
