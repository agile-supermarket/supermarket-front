#language: pt

Funcionalidade: Criar um funcionário

  Como um administrador do Cumpom Market EU QUERO cadastrar um funcionário a partir da página de Funcionário.

  Cenario: Cadastrar usuário sucesso
    Dado eu esteja na página de funcionário
    Quando eu clico em novo funcionário
    E a modal de cadastro de funcionário é mostrada
    E preencho todos os campos 
    E clico em salvar 
    Então eu quero que a modal de sucesso seja exibida 
    """
    Funcionário inserido com sucesso!
    """

  Cenário: Cadastrar usuário erro
    Dado eu esteja na página de funcionário
    Quando eu clico em novo funcionário
    E a modal de cadastro de funcionário é mostrada
    E não preencho o campo nome 
    E clico em salvar 
    Então eu quero que a modal de warning seja exibida 
    """
    Insira o nome do funcionário
    """

