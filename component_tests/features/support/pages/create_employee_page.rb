class CreateEmployeePage
  include Capybara::DSL

  INPUT_NAME = '[data-test-employee-name]'

  def fill_input(field, content)
    fill_in field, with: content, :wait => 2
  end

  def find_element(field, content)
    fill_in field, with: content, :wait => 2
  end

  def click_event(field)
    find(field, :wait => 5).click
  end

  def verify_success_modal
    find('.lead.text-muted').text
  end

  def verify_warning_modal
    find('.lead.text-muted').text
  end
end
